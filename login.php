<?php
/*
Plugin Name: RedirectAccessNotMyAccount
Plugin URI: https://gitlab.com/luism23/RedirectAccessNotMyAccount
Description: RedirectAccessNotMyAccount
Author: LuisMiguelNarvaez 
Version: 1.0.1
Author URI: https://gitlab.com/luism23
License: GPL
 */


    function redirectAccessNotMyAccount(){
        //Pasamos a la función home_url() el slug de nuestra página de Área Privada
        wp_redirect( home_url("area-privada") );
    
        exit();
    }
    add_action( 'wp_login','my_custom_login_redirect' );